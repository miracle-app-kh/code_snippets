<?php

/**
 * Bootstrap 4 Beta pagination function for WordPress
 * @author Miracle-app
 * @copyright 2017 Miracle-app
 */

if (!function_exists('miracle_pagination')) {
	function miracle_pagination() {
		global $wp_query;
		$big = 999999999;
		$links = paginate_links(array(
			'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))),
			'format' => '?paged=%#%',
			'current' => max(1, get_query_var('paged')),
			'type' => 'array',
			'prev_text'    => 'Previous',
			'next_text'    => 'Next',
			'total' => $wp_query->max_num_pages,
			'show_all'     => false,
			'end_size'     => 15,
			'mid_size'     => 15,
			'add_args'     => false,
			'add_fragment' => '',
			'before_page_number' => '',
			'after_page_number' => ''
		));

		if( is_array( $links ) ) {
			echo '<ul class="pagination pagination-lg">';
			foreach ( $links as $link ) {
				$link = str_replace('page-numbers', 'page-link', $link);
				if ( strpos( $link, 'current' ) !== false ) echo "<li class='page-item active'>$link</li>";
				else echo "<li class=\"page-item\">$link</li>";
			}
			echo '</ul>';
		}
	}
}